# OpenML dataset: Online_Sales

https://www.openml.org/d/46079

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Description:
The Online Sales Data.csv dataset is a comprehensive collection of sales transactions from an undisclosed online retailer. Spanning various regions and product categories, this dataset captures essential details of individual sales, offering insights into consumer behavior and sales performance across different markets. It includes information on transaction IDs, dates, product categories and names, units sold, unit prices, total revenue, geographical regions, and payment methods.

Attribute Description:
1. **Transaction ID**: A unique identifier for each sales transaction (e.g., 10032, 10138).
2. **Date**: The date when the transaction occurred, formatted as YYYY-MM-DD (e.g., 2024-03-30).
3. **Product Category**: Broad classification of the product sold (e.g., Beauty Products, Clothing).
4. **Product Name**: The specific name of the product sold (e.g., Bose QuietComfort 35 Headphones, Garmin Forerunner 945).
5. **Units Sold**: The quantity of the product that was sold in a single transaction (e.g., 5, 1).
6. **Unit Price**: The price of one unit of the product (e.g., 102.0, 199.99).
7. **Total Revenue**: The total income from the transaction (e.g., 299.99, 50.97).
8. **Region**: The geographical region where the sale was made (e.g., Europe, Asia, North America).
9. **Payment Method**: The method by which the transaction was paid (e.g., Credit Card).

Use Case:
This dataset is invaluable for analysts and researchers aiming to understand market trends, consumer preferences, and sales performance. Potential applications include analyzing seasonal variations in sales, comparing product performance across different regions, forecasting sales, and developing targeted marketing strategies. Additionally, insights derived from this dataset can drive inventory management decisions and personalize customer engagement efforts.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/46079) of an [OpenML dataset](https://www.openml.org/d/46079). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/46079/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/46079/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/46079/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

